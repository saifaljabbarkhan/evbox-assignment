import urllib.request
import re
import sys
from urllib.parse import urlparse

link = sys.argv[1]

val = urlparse(link)
if not re.search("http", val[0]):
    sys.exit("Argument not a valid URL")

f = urllib.request.urlopen(link)
myfile = f.read()
myfile = myfile.decode()
list1 = re.compile("<br />").split(myfile)

list2 = []

for x in list1:
    if x == '':
        list1.remove(x)
    elif not re.search(". OK$", x):
        list2.append(x)

if len(list2) !=0:
    for x in list2:
        list3 = re.compile(": ").split(x)
        print("The service %s is %s" % (list3[0], list3[1]))
else:
    print("All services are ok")
