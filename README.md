README

How to run:
	Execute the following command --- python serviceCheck.py URL
	Where URL is an argument

Library Choices:
	- urllib(parse/request) : Used to extract information from a webpage
	- re : Allows the use of regular expressions, helped in segragating the webpage content into a list and find the information that is needed.
	- sys : sys.argv allows the use of arguments to be given at command execution level

--------------------------------------------------------------------------------------------------------------------------
val = urlparse(link)
if not re.search("http", val[0]):
    sys.exit("Argument not a valid URL")

- Parsed the URL into a variable
- Check if scheme does not match http using regex if it does not match exit with non zero code

----------------------------------------------------------------------------------------------------------------------------
f = urllib.request.urlopen(link)
myfile = f.read()
myfile = myfile.decode()
list1 = re.compile("<br />").split(myfile)

- Request contents of URL page
- Read contents of the page
- decode contents into string format
- break string into list, using the html break as a divider

----------------------------------------------------------------------------------------------------------------------------
for x in list1:
    if x == '':
        list1.remove(x)
    elif not re.search(". OK$", x):
        list2.append(x)

- remove all list values that are empty (This prevents an error later on in the list when splitting the list values into tuples)
- Append any value that its service is not OK to the new list (The new list will only conist of services that are not okay and can be iterated through later on)

----------------------------------------------------------------------------------------------------------------------------
if len(list2) !=0:
    for x in list2:
        list3 = re.compile(": ").split(x)
        print("The service %s is %s" % (list3[0], list3[1]))
else:
    print("All services are ok")

- If statement checks if the list is empty
- for loop Iterates through the list splitting service and status into a tuple
- The service is then outputted with the status
- If the list is empty, the program outputs that the services are ok
